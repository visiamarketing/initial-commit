<?php

$EmailFrom = "webmaster@seniorsafeguard.com";
$EmailTo = "ssg@seniorsafeguard.com";
$Subject = "New message from website";
$Fname = Trim(stripslashes($_POST['Fname']));
$Lname = Trim(stripslashes($_POST['Lname']));
$Phone = Trim(stripslashes($_POST['Phone'])); 
$Email = Trim(stripslashes($_POST['Email'])); 
$Message = Trim(stripslashes($_POST['Message'])); 

// validation
$validationOK=true;
if (!$validationOK) {
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
  exit;
}

// prepare email body text
$Body = "You have a new message from the form on your contact webpage.";
$Body .= "\n";
$Body .= "\n";
$Body .= "First Name: ";
$Body .= $Fname;
$Body .= "\n";
$Body .= "Last Name: ";
$Body .= $Lname;
$Body .= "\n";
$Body .= "Phone Number: ";
$Body .= $Phone;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $Email;
$Body .= "\n";
$Body .= "Message: ";
$Body .= $Message;
$Body .= "\n";

// send email 
$success = mail($EmailTo, $Subject, $Body, "From: <$EmailFrom>");

// redirect to success page 
if ($success){
  print "<meta http-equiv=\"refresh\" content=\"0;URL=thank-you.html\">";
}
else{
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
}
?>